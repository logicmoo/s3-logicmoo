package s3.ai.basic.pathfinding;

import java.util.Comparator;


public class DLiteNode implements DLiteAbstractNode, Comparator<DLiteNode> {

	
	
	protected double _g;
	protected double _rhs;
	protected double _x;
	protected double _y;
	protected boolean nodeModified = false;

	protected MapPoint key;

	public MapPoint getKey() {
		return key;
	}

	public void setKey(MapPoint key) {
		this.key = key;
	}

	DLiteNode(MapPoint loc) {
		this.setDefaults();

		this.set_x(loc.m_a);
		this.set_y(loc.m_b);

	}
	
	public MapPoint getLoc() {
		return new MapPoint(this.get_x(), this.get_y());
	}

	DLiteNode(double x, double y) {

		this.set_x(x);
		this.set_y(y);

		this.setDefaults();
	}

	private void setDefaults() {
		this.set_g(Double.MAX_VALUE);
		this.set_rhs(Double.MAX_VALUE);

		double x = 0;
		double y = 0;
		this.setKey(new MapPoint(x, y));
	}

	public double get_g() {
		return _g;
	}

	public void set_g(double _g) {
		this._g = _g;
		
		if (_g == Double.MAX_VALUE && this.get_g() == 0){
			System.out.println("STOP");
		}
	}

	public double get_rhs() {
		return _rhs;
	}

	public void set_rhs(double _rhs) {
		this._rhs = _rhs;
		
		if (_rhs == Double.MAX_VALUE && this.get_rhs() == 0)
			System.out.println("STOP");
	}

	public double get_x() {
		return _x;
	}

	public void set_x(double _x) {
		this._x = _x;
	}

	public double get_y() {
		return _y;
	}

	public void set_y(double _y) {
		this._y = _y;
	}

	static double computeDistance(DLiteNode node1, DLiteNode node2) {
		return Math.sqrt((node1.get_x() - node2.get_x())
				* (node1.get_x() - node2.get_x())
				+ (node1.get_y() - node2.get_y())
				* (node1.get_y() - node2.get_y()));
	}

	static final double safeAdd(double left, double right) {
		if (right > 0 ? left > Double.MAX_VALUE - right
				: left < Double.MIN_VALUE - right) {
			// We are using MAX_VALUE to represent infinity.
			return Double.MAX_VALUE;
		}
		return left + right;
	}

	public boolean isSame(DLiteNode rhs) {
		if (this.get_x() == rhs.get_x() && this.get_y() == rhs.get_y())
			return true;
		else
			return false;
	}

	@Override
	public int compare(DLiteNode lhs, DLiteNode rhs) {

		return this.compare(lhs, rhs.getKey());
	}

	public int compare(DLiteNode lhs, MapPoint rhs) {

		if (lhs.getKey().m_a < rhs.m_a)
			return -1;
		else if (lhs.getKey().m_a > rhs.m_a)
			return 1;
		else {
			// They are equal
			if (lhs.getKey().m_b < rhs.m_b)
				return -1;
			else if (lhs.getKey().m_b < rhs.m_b)
				return 1;
			else
				return 0;
		}

	}
	
	public String toString() {
		return new String("x: " + this.get_x() + " y: " + this.get_y());
	}

	public boolean isModified() {
		boolean value = this.nodeModified;
		
		if (value) {
			//If it's modified, it's being viewed, so flip back to unmodified
			this.nodeModified = false;
		}
		
		return value;
			
	}
	
	public void setModified(){
		this.nodeModified = true;
	}
}
