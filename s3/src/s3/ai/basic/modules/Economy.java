package s3.ai.basic.modules;

import java.util.LinkedList;
import java.util.List;

import s3.ai.basic.Request;
import s3.base.S3Action;
import s3.entities.WOTree;
import s3.entities.WPeasant;
import s3.entities.WTownhall;
import s3.entities.WUnit;

public class Economy extends AIModule {

	protected Logistics logistics;
	protected Perception perception;

	/**
	 * @param l
	 *            logistics module instance. The Economy module will make
	 *            requests to this module for units.
	 * @param p
	 *            perception module instance
	 */
	Economy(Logistics l, Perception p) {
		this.logistics = l;
		this.perception = p;
	}

	public List<Request> setGoal(int numWorkersWood, int numWorkersGold) {

		// Create the list of actions
		List<Request> requests = new LinkedList<Request>();
		int townHallPriority = 100;
		int minePriority = 100;
		int chopPriority = 100;
		WPeasant idlePeasant = null;

		// Get the townhall instance
		LinkedList<WTownhall> thList = (LinkedList) this.perception
				.getExistingUnitsOfType(WTownhall.class);

		int numOfPeasants = this.perception.getNumberOf(WPeasant.class);

		if (!thList.isEmpty()) {
			WTownhall th = thList.pop();
			
			
			if (numOfPeasants < numWorkersWood + numWorkersGold + 1) {
				// The plus one is to ensure there is always a spare peasant to
				// build things
				if (th.isIdle())

					requests.add(this.getPeasantTrainRequest(townHallPriority,
							th));
			}
		} else {
			// Create a townhall
			try {
				requests.addAll(this.logistics.request(WTownhall.class,
						townHallPriority));
			} catch (NullPointerException e) {
				if (e.toString().contains("satisfied")) {
					// This is ok, do nothing
				}
			}
		}

		List<WPeasant> unusedPeasants = new LinkedList<WPeasant>();

		while ((idlePeasant = this.perception.getNextIdlePeasant()) != null) {
			if (this.perception.getNumPeasantsGold() < numWorkersGold
					&& null != this.perception.findClosestMineTo(idlePeasant)) {

				requests.add(this.getMineRequest(minePriority, idlePeasant,
						this.perception.findClosestMineTo(idlePeasant)));
			}

			else if (this.perception.getNumPeasantsWood() < numWorkersWood) {
				WOTree tree = this.getClosestTreeTo(idlePeasant);

				if (null != tree) {
					requests.add(this.getChopRequest(chopPriority, idlePeasant,
							tree));
				}

			} else {
				// be fair and add this unused peasant back to the pool
				unusedPeasants.add(idlePeasant);
			}
		}

		// Have to return these unused peasants or other modules will think they
		// are busy
		if (!unusedPeasants.isEmpty()) {
			this.perception.returnIdlePeasants(unusedPeasants);
		}

		return requests;

	}

	protected WOTree getClosestTreeTo(WPeasant peasant) {

		List<WOTree> trees = this.perception.getTrees();

		WOTree tree = null;
		int leastDist = 9999;
		for (WOTree unit : trees) {
			int dist = Math.abs(unit.getX() - peasant.getX())
					+ Math.abs(unit.getY() - peasant.getY());
			if (dist < leastDist) {
				leastDist = dist;
				tree = unit;
			}
		}

		return tree;

	}

}
