package s3.ai.basic.modules;

import java.util.List;

import s3.base.S3;
import s3.entities.S3PhysicalEntity;
import s3.util.Pair;

/**
 * Interface for generic path finder algorithms
 * 
 */
public interface PathFinder {

	/**
	 * Calculates the path from start to goal.
	 * 
	 * @return Returns the List of coordinates along the computed path from
	 *         start to goal
	 */
	public List<Pair<Double, Double>> computePath();

	/**
	 * Calculates the path distance from the start to the finish, for the
	 * specified entity
	 * 
	 * @param start_x
	 *            The starting x coordinate to start
	 * @param start_y
	 *            The starting y coordinate to start
	 * @param goal_x
	 *            the goal x coordinate
	 * @param goal_y
	 *            the goal y coordinate
	 * @param i_entity
	 *            the S3 entity for which, this instance is planning a path
	 * @param the_game
	 *            instance of the game
	 * @return the number of points in the path (i.e. the size of the path)
	 */
	public int pathDistance(double start_x, double start_y, double goal_x,
			double goal_y, S3PhysicalEntity i_entity, S3 the_game);

	
	public List<Pair<Double,Double>>replan(double current_x, double current_y );
}
