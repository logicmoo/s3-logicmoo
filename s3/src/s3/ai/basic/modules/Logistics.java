package s3.ai.basic.modules;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import s3.ai.basic.Request;
import s3.base.S3Action;
import s3.entities.*;

public class Logistics extends AIModule {

	static final HashMap<Class<? extends WUnit>, List<Class<? extends WUnit>>> unitDependencies = new HashMap<Class<? extends WUnit>, List<Class<? extends WUnit>>>();

	public static final List<Class<? extends WUnit>> buildings = new LinkedList<Class<? extends WUnit>>();

	public static final List<Class<? extends WTroop>> troops = new LinkedList<Class<? extends WTroop>>();
	
	public static final int TOWN_HALL_PRIORITY = 200;

	static {
		// Create the list
		List<Class<? extends WUnit>> unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WTownhall.class);
		// Barracks depends on Townhall
		unitDependencies.put(WBarracks.class, unitList);

		// Lumbermill depends on Townhall
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WTownhall.class);
		unitDependencies.put(WLumberMill.class, unitList);

		// Blacksmith depends on Townhall
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WTownhall.class);
		unitDependencies.put(WBlacksmith.class, unitList);

		// Tower depends on Lumbermill
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WLumberMill.class);
		unitDependencies.put(WTower.class, unitList);

		// Fortress depends on Barracks, Lumbermill and Blacksmith
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WBarracks.class);
		unitList.add(WLumberMill.class);
		unitList.add(WBlacksmith.class);
		unitDependencies.put(WFortress.class, unitList);

		// Stable depends on Fortress
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WFortress.class);
		unitDependencies.put(WStable.class, unitList);

		// Now add the Troops (fighting units)

		// Footmen depend on Barracks
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WBarracks.class);
		unitDependencies.put(WFootman.class, unitList);

		// Archers depend on LumberMills
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WLumberMill.class);
		unitList.add(WBarracks.class);
		unitDependencies.put(WArcher.class, unitList);

		// Catapults depend on Blacksmith and lumbermill
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WBlacksmith.class);
		unitList.add(WLumberMill.class);
		unitList.add(WBarracks.class);
		unitDependencies.put(WCatapult.class, unitList);

		// Knights depend on stable
		unitList = new LinkedList<Class<? extends WUnit>>();
		unitList.add(WStable.class);
		unitDependencies.put(WKnight.class, unitList);

		// Create the lists of buildings and troops
		buildings.add(WTownhall.class);
		buildings.add(WLumberMill.class);
		buildings.add(WBarracks.class);
		buildings.add(WBlacksmith.class);
		buildings.add(WFortress.class);
		buildings.add(WWall.class);

		troops.add(WFootman.class);
		troops.add(WArcher.class);
		troops.add(WCatapult.class);
		troops.add(WKnight.class);

	}

	protected Arbiter arbiter;
	protected Perception perception;

	Logistics(Arbiter a, Perception p) {
		this.arbiter = a;
		this.perception = p;
	}

	protected boolean isUnitBuilt(Class<? extends WUnit> unitType) {
		for (WUnit unit : this.perception.getExistingUnitsOfType(unitType)) {
			if (unitType.isInstance(unit)) {
				return true;
			}
		}

		return false;
	}

	protected Request getIdlePeasantAndBuild(
			Class<? extends WBuilding> building, int priorityRequest)
			throws NullPointerException {

		Request request = null;

		WPeasant idlePeasant = this.perception.getNextIdlePeasant();

		if (null != idlePeasant) {

			if (WTownhall.class.isAssignableFrom(building)) {

				// Special behavior for th

				request = this.getBuildRequest(Logistics.TOWN_HALL_PRIORITY, idlePeasant,
						WTownhall.class, this.perception.findFreeSpaceAround(
								this.perception.findClosestMineTo(idlePeasant),
								4, 3));
			} else {

				// There is an idle peasant around to build the
				// building, and the building is not the town hall

				request = this.getBuildRequest(priorityRequest + 1,
						idlePeasant, building,
						this.perception.findFreeSpaceAround(idlePeasant, 4, 3));

			}
		} else {
			// There are no idle peasants

			throw new NullPointerException("Request for a "
					+ building.getSimpleName()
					+ " can't be satisfied.  There are no idle peasants.");
		}

		return request;

	}
	
	protected Request getIdleBarracksAndTrain(Class<? extends WTroop> trooper, int priorityRequest)
			throws NullPointerException {
		
		Request request = null;
		
		WBarracks barracks = this.perception.getNextIdleBarracks();


		if (barracks != null) {
			request = this.perception.getTroopTrainRequest(priorityRequest,
					barracks, trooper);
		}
		else {
			throw new NullPointerException("No idle Barracks");
		}
		
		return request;

	}

	protected List<Request> getBuildingDepends(
			Class<? extends WUnit> unit, int priorityRequest)
			throws NullPointerException {

		List<Class<? extends WUnit>> dependicies = Logistics.unitDependencies
				.get(unit);

		List<Request> requests = new LinkedList<Request>();

		boolean wasExceptionThrown = false;

		// Two cases, there are no depends, or this building depends on
		// something

		if (null == dependicies) {
			// No depends

			// Does the building already exist?
			if (0 == this.perception.getNumberOf(unit) && WBuilding.class.isAssignableFrom(unit)) {

				// Try to Build it
				
				Class<WBuilding> building = (Class<WBuilding>) unit;

				try {
					requests.add(this.getIdlePeasantAndBuild(building, priorityRequest));
				} catch (NullPointerException e) {
					// No Idle peasants
					throw new NullPointerException(e.getMessage());
				}

			} else {
				// The building already exists, nothing else to do.  Or the unit is a troop, so just return
				return requests;
			}
		}

		else {

			// Else there are depends
			for (Class<? extends WUnit> dependsOn : dependicies) {
				
				if (!this.isUnitBuilt(dependsOn)) {

					try {
						requests.addAll(this.getBuildingDepends(dependsOn, priorityRequest));
					} catch (NullPointerException e) {
						wasExceptionThrown = true;
					}

				}
			}

			// Now we should have the list of depends in the form of requests,
			// but there are a couple of cases

			if (requests.isEmpty() && !wasExceptionThrown && WBuilding.class.isAssignableFrom(unit)) {
				// There are no subordinate requests, and there was no
				// exception, it is safe to build this current request
				Class<WBuilding> building = (Class<WBuilding>) unit;
				try {
					requests.add(this.getIdlePeasantAndBuild(building, priorityRequest));
				} catch (NullPointerException e) {
					// No Idle peasants
					throw new NullPointerException(e.getMessage());
				}

			} else if (requests.isEmpty() && wasExceptionThrown) {
				// No subordinate requests, but can't build this item b/c there
				// was a problem
				//So throw an exception
				throw new NullPointerException("Can't build " + unit.getSimpleName() + " yet.");
			} else {
				// requests were not empty, but there may be an exception.
				// Either way we can return the requests, because we can't build
				// the original request yet
				return requests;
			}

		}

		return requests;
	}
	
	public List<Request> request(Class<? extends WUnit> unit,
			int priorityRequest) throws NullPointerException {
		
		List<Request> requests = new LinkedList<Request>();
		
		//Is this a building, a peasant, or a troop?
		if (WPeasant.class.isAssignableFrom(unit)){
			
		}
		else if (WTroop.class.isAssignableFrom(unit)){
			try {
				requests.addAll(this.getBuildingDepends(unit, priorityRequest));
				
				if (requests.isEmpty()){
					//train the toop
					Class<WTroop> trooper = (Class<WTroop>)unit;
					requests.add(this.getIdleBarracksAndTrain(trooper, priorityRequest));
				}
				else {
					//There are requests in the queue, we must build those first
				}
			}
			catch (NullPointerException e){
				//Can't be built
			}
			
		}
		else if (WBuilding.class.isAssignableFrom(unit)){
			try {
				requests.addAll(this.getBuildingDepends(unit, priorityRequest));
				
			}
			catch (NullPointerException e){
				//Can't be built
			}
			
				
		}
		else {
			throw new NullPointerException("Invalid request");
		}
		

		
		this.removeDupes(requests);
		
		return requests;
	}	
	
	protected void removeDupes(List<Request> requests) {
		List<Request> dupes = new LinkedList<Request>();

		for (Request r1 : requests) {

			if (S3Action.ACTION_BUILD == r1.action.m_action) {

				String s = r1.action.m_parameters.get(0).toString();

				for (Request r2 : requests) {

					if (S3Action.ACTION_BUILD == r2.action.m_action && r1 != r2) {

						String s2 = r2.action.m_parameters.get(0).toString();

						if (s == s2) {
							System.out.println("Trying to build dupe: " + s);
							dupes.add(r2);
						}
					}
				}
			}
		}

		requests.removeAll(dupes);
	}


}
