package s3.ai.basic.modules;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

import s3.base.S3;
import s3.base.S3Action;
import s3.entities.*;
import s3.util.Pair;

public class Perception extends AIModule {

	protected LinkedList<WPeasant> idlePeasants;
	protected LinkedList<WBarracks> idleBarracks;
	protected WPlayer player;
	protected S3 game;
	protected HashMap<Class<? extends WTroop>, List<WTroop>> idleTroops;
	protected int wavesCompleted = 0;

	public Perception() {
		// Default constructor
	}

	public void increaseWaveCompleted() {
		this.wavesCompleted++;
	}

	public int getWavesCompleted() {
		return this.wavesCompleted;
	}

	public void update(S3 game, WPlayer player) {
		// Refresh objects for this turn

		this.idlePeasants = new LinkedList<WPeasant>();
		this.idleBarracks = new LinkedList<WBarracks>();
		this.player = player;
		this.game = game;

		// Fist refresh idle peasants
		for (WUnit unit : this.getExistingUnitsOfType(WPeasant.class)) {
			WPeasant peasant = (WPeasant) unit;
			if (peasant.getStatus() != null){
				if (peasant.getStatus().m_action == S3Action.ACTION_HARVEST && peasant.getStatus().m_parameters.size() != 1){
					//Wood gather, check to see if he is "really busy"

					
				}


			}
			if (peasant.isIdle()) {
				// Peasant should not be doing anything
				idlePeasants.push(peasant);

			}
		}

		// Refresh idle barracks
		for (WUnit unit : this.getExistingUnitsOfType(WBarracks.class)) {
			WBarracks barracks = (WBarracks) unit;
			if (barracks.isIdle()) {
				this.idleBarracks.add(barracks);
			}
		}

		// Refresh idle troops

		// First create the idle troop map
		this.idleTroops = new HashMap<Class<? extends WTroop>, List<WTroop>>();

		// First add empty troops to the list
		for (Class<? extends WTroop> troop : Logistics.troops) {
			List<WTroop> emptyList = new LinkedList<WTroop>();

			this.idleTroops.put(troop, emptyList);

		}

		// Now count up the existing troops
		List<WUnit> troops = this.getAllFightingUnits();

		for (WUnit existingTroop : troops) {
			for (Class<? extends WTroop> troop : Logistics.troops) {
				List<WTroop> troopList = this.idleTroops.get(troop);

				if (troop.isInstance(existingTroop)
						&& (existingTroop.getStatus() == null || existingTroop
								.getStatus().m_action == S3Action.ACTION_STAND_GROUND)) {

					troopList.add((WTroop) existingTroop);
				}

			}
		}

		// Refresh Building list

		// TODO gather stats about the army

	}

	/**
	 * Returns an idle peasant. This method should be used to prevent peasant
	 * collision.
	 * 
	 * @return An instance of an peasant that is idle. This way prevents
	 *         "peasant collision", or having multiple idle peasants assigned to
	 *         a task.
	 */
	public WPeasant getNextIdlePeasant() {
		WPeasant peasant = null;

		if (!this.idlePeasants.isEmpty()) {
			peasant = this.idlePeasants.pop();
		}

		return peasant;
	}

	public void returnIdlePeasants(List<WPeasant> unusedPeasants) {
		for (WPeasant p : unusedPeasants) {
			this.idlePeasants.add(p);
		}
	}

	public int getNumPeasantsGold() {

		int gp = 0;

		for (WUnit peasant : this.getExistingUnitsOfType(WPeasant.class)) {

			WPeasant p = (WPeasant) peasant;

			if (p.isGatheringGold())
				gp++;
		}

		return gp;

	}

	public int getNumPeasantsWood() {

		int wp = 0;

		for (WUnit peasant : this.getExistingUnitsOfType(WPeasant.class)) {

			WPeasant p = (WPeasant) peasant;

			if (p.isGatheringWood()) {
				wp++;
			}
		}

		return wp;

	}

	public WBarracks getNextIdleBarracks() {
		WBarracks barracks = null;

		if (!this.idleBarracks.isEmpty()) {
			barracks = this.idleBarracks.pop();
		}

		return barracks;
	}

	/**
	 * Finds free space around the given unit. This will first try the desired
	 * space and then try the next lower space, until it equals the min space
	 * parameter.
	 * 
	 * @param unit
	 *            The unit around which free space is found
	 * @param desiredSpace
	 *            The desired amount of space, it is implied that it is
	 *            desiredSpace x desiredSpace. i.e. if desiredSpace is 4, the
	 *            caller is looking for a 4x4 patch of free space
	 * @param minSpace
	 *            The minimum amount of space desired
	 * @return The location for free space
	 * @throws NullPointerException
	 *             If no space is found, or if the input parameters didn't make
	 *             sence, this excetpion is thrown.
	 */
	public Pair<Integer, Integer> findFreeSpaceAround(S3PhysicalEntity unit,
			int desiredSpace, int minSpace) throws NullPointerException {

		Pair<Integer, Integer> loc = null;

		if (minSpace > desiredSpace || minSpace < 0 || desiredSpace < 0) {
			// Some basic range checks

		} else {

			for (int space = desiredSpace; space >= minSpace; space--) {
				loc = this.game.findFreeSpace(unit.getX(), unit.getY(),
						space);

				if (null != loc) {
					break;
				}
			}

		}

		if (null == loc) {
			throw new NullPointerException("There is no free space around "
					+ unit.toString() + "\n");
		}

		return loc;
	}

	/**
	 * Wrapper for the S3 game get unit types function. This should not be
	 * called directly to get idle units, instead use the appropiate get idle
	 * unit methods in this class.
	 * 
	 * @param unitType
	 * @return
	 */
	public List<WUnit> getExistingUnitsOfType(Class<? extends WUnit> unitType) {
		return this.game.getUnitTypes(this.player, unitType);
	}

	public int getNumberOfType(Class<? extends WUnit> unitType) {
		List<WUnit> units = this.getExistingUnitsOfType(unitType);

		return units.size();
	}

	public WUnit getExistingUnit(Class<? extends WUnit> unitType) {
		return this.game.getUnitType(player, unitType);
	}

	public int getNumberOf(Class<? extends WUnit> unitType) {
		return this.getExistingUnitsOfType(unitType).size();
	}

	public int getAvailableGold() {
		return this.player.getGold();
	}

	public int getAvailableWood() {
		return this.player.getWood();
	}

	public WGoldMine findClosestMineTo(WPeasant peasant) {
		int leastDist = 9999;

		List<WUnit> mines = this.getMines();

		WGoldMine mine = null;

		for (WUnit unit : mines) {
			int dist = peasant.distance(unit, this.game);
			if (dist != -1 && dist < leastDist
					&& ((WGoldMine) unit).getRemaining_gold() > 0) {
				leastDist = dist;
				mine = (WGoldMine) unit;
			}
		}

		return mine;
	}

	public WUnit findClosestUnitTo(List<WUnit> unitList, WTroop targetUnit) {
		int leastDist = 9999;

		//TODO Changed this to get the smallest distance based on heuristic value
		WUnit closestUnit = null;

		for (WUnit unit : unitList) {
			int dist = targetUnit.distance(unit, this.game);
			if (dist != -1 ) {
				return unit;
			}
		}
		
		return null;
		
		//Finds the closest unit
//		for (WUnit unit : unitList) {
//			int dist = targetUnit.distance(unit, this.game);
//			if (dist != -1 && dist < leastDist) {
//				leastDist = dist;
//				closestUnit = unit;
//			}
//		}
//
//		return closestUnit;

	}

	public List<WUnit> getMines() {
		List<WUnit> mines = this.game.getUnitTypes(null, WGoldMine.class);

		return mines;
	}

	public List<WOTree> getTrees() {
		List<WOTree> trees = new LinkedList<WOTree>();
		for (int i = 0; i < game.getMap().getWidth(); i++) {
			for (int j = 0; j < game.getMap().getHeight(); j++) {
				S3PhysicalEntity e = game.getMap().getEntity(i, j);
				if (e instanceof WOTree)
					trees.add((WOTree) e);
			}
		}

		return trees;
	}

	public HashMap<Class<? extends WTroop>, List<WTroop>> getIdleTroops() {
		return this.idleTroops;
	}

	public List<WUnit> getAllFightingUnits() {
		List<WUnit> listOfUnits = new LinkedList<WUnit>();

		for (Class<? extends WTroop> troop : Logistics.troops) {
			listOfUnits.addAll(this.getExistingUnitsOfType(troop));
		}

		return listOfUnits;

	}

	public List<WUnit> getEnemyUnits() {

		List<WUnit> enemies = new LinkedList<WUnit>();

		WPlayer enemy = null;
		for (WPlayer entity : game.getPlayers()) {
			if (entity != player) {
				enemy = entity;
				break;
			}
		}

		Class[] targetClasses = { WCatapult.class, WKnight.class,
				WArcher.class, WFootman.class, WPeasant.class, WTower.class,
				WTownhall.class, WBarracks.class, WLumberMill.class,
				WBlacksmith.class, WFortress.class, WStable.class };

		WUnit enemyTroop = null;

		for (Class c : targetClasses) {
			WUnit e = game.getUnitType(enemy, c);
			if (e != null) {
				enemies.add(e);
			}

		}

		return enemies;
	}

}
